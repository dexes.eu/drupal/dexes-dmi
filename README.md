# Dexes / Drupal / Dexes DMI

[gitlab.com/dexes.eu/drupal/dexes-dmi](https://gitlab.com/dexes.eu/drupal/dexes-dmi)

The suite of modules required to integrate various Dexes DMI components.

## License

View the `LICENSE.md` file for licensing details.

## Installation
// TODO add packagist url
Installation of [`dexes-drupal/dexes_dmi`]() is done via [Composer](https://getcomposer.org).

```shell
composer require dexes-drupal/dexes_dmi
```
