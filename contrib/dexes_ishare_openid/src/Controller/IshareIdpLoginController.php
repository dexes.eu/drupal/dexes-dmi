<?php

declare(strict_types=1);

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_ishare_openid\Controller;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\Core\Url;
use Drupal\openid_connect\OpenIDConnectClaims;
use Drupal\openid_connect\OpenIDConnectSession;
use Drupal\openid_connect\Plugin\OpenIDConnectClientInterface;
use Drupal\openid_connect\Plugin\OpenIDConnectClientManager;
use LogicException;
use RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UnexpectedValueException;

/**
 * Class IshareIdpLoginController.
 *
 * Drupal controller for authenticating users via OAuth.
 */
final class IshareIdpLoginController extends ControllerBase
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var OpenIDConnectClientManager $openIdConnectClientManager */
    $openIdConnectClientManager = $container->get('plugin.manager.openid_connect_client');

    /** @var OpenIDConnectClaims $openIdConnectClaims */
    $openIdConnectClaims = $container->get('openid_connect.claims');

    /** @var OpenIDConnectSession $openIdConnectSession */
    $openIdConnectSession = $container->get('openid_connect.session');

    /** @var SessionManagerInterface $sessionManager */
    $sessionManager = $container->get('session_manager');

    /** @var RendererInterface $renderer */
    $renderer = $container->get('renderer');

    /** @var PathValidatorInterface $pathValidator */
    $pathValidator = $container->get('path.validator');

    return new self($openIdConnectClientManager, $openIdConnectClaims, $openIdConnectSession, $sessionManager, $renderer, $pathValidator);
  }

  /**
   * IshareIdpLoginController constructor.
   *
   * @param OpenIDConnectClientManager $pluginManager  The plugin manager
   * @param OpenIDConnectClaims        $claims         The OpenID Connect claims
   * @param OpenIDConnectSession       $session        The openID connect session instance
   * @param SessionManagerInterface    $sessionManager The Drupal session manager interface
   * @param RendererInterface          $renderer       The Drupal renderer interface
   * @param PathValidatorInterface     $pathValidator  The Drupal path validator interface
   */
  public function __construct(protected readonly OpenIDConnectClientManager $pluginManager,
                              protected readonly OpenIDConnectClaims $claims,
                              protected readonly OpenIDConnectSession $session,
                              protected readonly SessionManagerInterface $sessionManager,
                              protected readonly RendererInterface $renderer,
                              protected readonly PathValidatorInterface $pathValidator,
  ) {
  }

  /**
   * Authenticate a user via OpenID Connect.
   *
   * @param Request $request the current HTTP request
   *
   * @return Response by the OpenID Connect client during the authentication process, or redirect response if the user is already authenticated
   *
   * @throws PluginException thrown when an error occurs in the plugin manager while retrieving the OpenID Connect client
   * @throws LogicException  in case bubbling has failed
   */
  public function login(Request $request): Response
  {
    if ($this->currentUser()->isAuthenticated()) {
      return $this->redirect('user.page');
    }

    $this->initSession();

    $renderContext = new RenderContext();

    return $this->renderer->executeInRenderContext($renderContext, function() use ($request) {
      $client = $this->getOpenIdClient();
      $this->prepareSession($request);

      return $client->authorize($this->claims->getScopes($client));
    });
  }

  /**
   * Initializes the session.
   */
  private function initSession(): void
  {
    if (!$this->sessionManager->isStarted()) {
      $this->sessionManager->start();
    }
  }

  /**
   * Create and return a configured OpenId client.
   *
   * @return OpenIDConnectClientInterface The created client
   *
   * @throws PluginException Thrown on any error from the plugin manager
   */
  private function getOpenIdClient(): OpenIDConnectClientInterface
  {
    /** @var OpenIDConnectClientInterface $client */
    $client = $this->pluginManager->createInstance(
      'ishare_idp',
      $this->config('openid_connect.settings.ishare_idp')->get('settings')
    );

    if (!$client instanceof OpenIDConnectClientInterface) {
      throw new RuntimeException('plugin \'ishare_idp\' did not resolve to an OpenIDConnectClientInterface instance');
    }

    return $client;
  }

  /**
   * Prepare the `$_SESSION` variable for the OpenID authentication process.
   *
   * @param Request $request the current HTTP request
   */
  private function prepareSession(Request $request): void
  {
    $_SESSION['openid_connect_op'] = 'login';

    $destinationPath = $this->getRefererPath($request);
    if (!$destinationPath) {
      $this->session->saveDestination();

      return;
    }

    $_SESSION['openid_connect_destination'] = [
      $destinationPath,
      [
        'query' => $request->getQueryString(),
      ],
    ];
  }

  /**
   * Retrieves the referer URL from the HTTP request.
   *
   * @param Request $request the current HTTP request
   *
   * @return ?string the referer URL if present, or null if not available
   */
  private function getRefererUrl(Request $request): ?string
  {
    return $request->server->get('HTTP_REFERER');
  }

  /**
   * Creates and returns a validated Url object from the given URL string.
   *
   * @param string $url the URL string to validate
   *
   * @return ?Url a validated Url object if the URL is valid, or null otherwise
   */
  private function getUrlObject(string $url): ?Url
  {
    $request = Request::create($url);

    return $this->pathValidator->getUrlIfValid($request->getRequestUri()) ?: NULL;
  }

  /**
   * Retrieves the internal path from the given Url object.
   *
   * @param Url $urlObject the Url object from which to extract the internal path
   *
   * @return string the internal path associated with the Url object
   *
   * @throws UnexpectedValueException if the URI is with no corresponding route
   */
  private function getUrlObjectInternalPath(Url $urlObject): string
  {
    $url = Url::fromRoute($urlObject->getRouteName(), $urlObject->getRouteParameters());

    return $url->getInternalPath();
  }

  /**
   * Validates a URL string and returns its internal path if valid.
   *
   * @param string $url the URL string to validate
   *
   * @return ?string the internal path of the URL if it is valid and not blocked, or null otherwise
   */
  private function getValidatedUrlInternalPath(string $url): ?string
  {
    try {
      $urlObject = $this->getUrlObject($url);

      if (!$urlObject || $this->isBlockedRoute($urlObject->getRouteName())) {
        return NULL;
      }

      return $this->getUrlObjectInternalPath($urlObject);
    } catch (UnexpectedValueException $e) {
      $this->getLogger('dexes_ishare_openid')->error(sprintf('Failed to process referer URL: %s', $e->getMessage()));

      return NULL;
    }
  }

  /**
   * Retrieves the internal path of the referer URL from the given request.
   *
   * @param Request $request the current HTTP request
   *
   * @return ?string the internal path of the referer URL if it is valid, or null if no referer exists or validation fails
   */
  private function getRefererPath(Request $request): ?string
  {
    $refererUrl = $this->getRefererUrl($request);

    if (empty($refererUrl)) {
      return NULL;
    }

    return $this->getValidatedUrlInternalPath($refererUrl);
  }

  /**
   * Determines whether the provided route name is blocked.
   *
   * @param string $routeName the route name to check
   *
   * @return bool true if the route is blocked, false otherwise
   */
  private function isBlockedRoute(string $routeName): bool
  {
    return 'openid_connect.redirect_controller_redirect' === $routeName;
  }
}
