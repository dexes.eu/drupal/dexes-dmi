<?php

declare(strict_types=1);

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_ishare_openid\Plugin\Menu;

use Drupal\user\Plugin\Menu\LoginLogoutMenuLink;

/**
 * Class OpenIdIshareIdpLogoutMenuLink.
 *
 * Alters the standard Drupal user menu to directly point to the OpenID authentication endpoint rather than the generic
 * authentication page.
 */
final class OpenIdIshareIdpLogoutMenuLink extends LoginLogoutMenuLink
{
  /**
   * {@inheritdoc}
   */
  public function getRouteName(): string
  {
    if ($this->currentUser->isAuthenticated()) {
      return 'user.logout';
    }

    return 'dexes_ishare_openid.ishare_idp_login';
  }
}
