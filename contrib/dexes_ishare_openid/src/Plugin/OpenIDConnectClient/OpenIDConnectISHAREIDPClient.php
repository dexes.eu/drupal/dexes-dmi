<?php

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_ishare_openid\Plugin\OpenIDConnectClient;

use Drupal;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\dexes_ishare\DexesIShare;
use Drupal\dexes_ishare\JWT;
use Drupal\dexes_ishare\ManagedSettingsTrait;
use Drupal\openid_connect\Plugin\OpenIDConnectClientBase;
use Exception;
use Firebase\JWT\JWT as FirebaseJWT;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * iSHARE IDP OpenID Connect client.
 *
 * Used primarily to login to Drupal sites powered by oauth2_server or PHP
 * sites powered by oauth2-server-php.
 *
 * @OpenIDConnectClient(
 *   id = "ishare_idp",
 *   label = @Translation("iSHARE IDP")
 * )
 */
class OpenIDConnectISHAREIDPClient extends OpenIDConnectClientBase
{
  use ManagedSettingsTrait;

  private const ALGORITHM = 'RS256';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self
  {
    /** @var ConfigFactoryInterface $configFactory */
    $configFactory                             = $container->get('config.factory');
    $ishareConfiguration                       = $configFactory->get(DexesIShare::SETTINGS_KEY);
    $configuration['client_certificate_chain'] = $ishareConfiguration->get('certificate_chain');
    $configuration['client_secret']            = $ishareConfiguration->get('private_key');
    $configuration['client_id']                = $ishareConfiguration->get('client_id');

    return parent::create($container, $configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array
  {
    return [
      'idp_endpoint'             => '',
      'client_certificate_chain' => '',
      'idp_eori'                 => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array
  {
    $form['idp_eori'] = [
      '#title'         => t('IDP EORI'),
      '#type'          => 'textfield',
      '#default_value' => $this->configuration['idp_eori'] ?? '',
    ];

    $form['idp_endpoint'] = [
      '#title'         => $this->t('IDP endpoint'),
      '#type'          => 'textfield',
      '#default_value' => $this->configuration['idp_endpoint'],
    ];

    $form = parent::buildConfigurationForm($form, $form_state);

    $form['client_id']['#title']    = t('Client EORI');
    $form['client_id']['#disabled'] = TRUE;

    unset($form['client_secret']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void
  {
    // Get plugin setting values.
    $configuration = $form_state->getValues();

    if (empty($this->configuration['client_certificate_chain']) || empty($this->configuration['client_secret']) || empty($this->configuration['client_id'])) {
      $form_state->setErrorByName('client_id', $this->t('The Dexes iSHARE Settings are empty, please contact an administrator.'));
    }

    if (empty($configuration['idp_eori'])) {
      $form_state->setErrorByName('idp_eori', $this->t('The IDP EORI is missing for iSHARE idp.'));
    }

    if (empty($configuration['idp_endpoint'])) {
      $form_state->setErrorByName('idp_endpoint', $this->t('The IDP endpoint is missing for iSHARE idp.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoints(): array
  {
    $idpEndpoint = $this->configuration['idp_endpoint'];

    return [
      'authorization' => $idpEndpoint . '/openid-connect/auth',
      'token'         => $idpEndpoint . '/openid-connect/token',
      'userinfo'      => $idpEndpoint . '/openid-connect/userinfo',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function authorize($scope = 'openid ishare'): TrustedRedirectResponse|Response
  {
    $response = parent::authorize($scope);
    if ($response instanceof TrustedRedirectResponse) {
      $url = str_replace('openid%2Bishare', 'openid+ishare', $response->getTargetUrl());

      $response->setTrustedTargetUrl($url);
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveUserInfo($access_token)
  {
    $request_options = [
      'headers' => [
        'Authorization' => 'Bearer ' . $access_token,
        'Accept'        => 'application/json',
      ],
    ];
    $endpoints = $this->getEndpoints();

    $client = $this->httpClient;

    try {
      $response      = $client->request('GET', $endpoints['userinfo'], $request_options);

      $response_data = (string) $response->getBody();

      $response_data = JWT::decode($response_data);

      return $response_data['payload'];
    } catch (Exception $e) {
      $variables = [
        '@message'       => 'Could not retrieve user profile information',
        '@error_message' => $e->getMessage(),
      ];

      if ($e instanceof RequestException && $e->hasResponse()) {
        $response_body = $e->getResponse()?->getBody()?->getContents();
        $variables['@error_message'] .= ' Response: ' . $response_body;
      }

      $this->loggerFactory->get('openid_connect_' . $this->pluginId)
        ->error('@message. Details: @error_message', $variables);

      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getRequestOptions($authorization_code, $redirect_uri): array
  {
    return [
      'form_params' => [
        'grant_type'       => 'authorization_code',
        'redirect_uri'     => $redirect_uri,
        'client_id'        => $this->configuration['client_id'],
        'client_assertion' => $this->configuration['client_secret'],
        'code'             => $authorization_code,
      ],
      'headers' => [
        'Accept'       => 'application/json',
        'Content-Type' => 'application/x-www-form-urlencoded;charset=UTF-8',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getUrlOptions($scope, GeneratedUrl $redirect_uri): array
  {
    $currentTime = Drupal::time()->getCurrentTime();
    $privateKey  = $this->configuration['client_secret'];
    /** @var array<string, string> $head */
    $head = [
      'typ' => 'JWT',
      'alg' => self::ALGORITHM,
      'x5c' => $this->getX5CArray(),
    ];
    $body = [
      'client_id'     => $this->configuration['client_id'],
      'scope'         => 'openid ishare',
      'redirect_uri'  => $this->getRedirectUrl()->toString(),
      'response_type' => 'code',
      'iat'           => $currentTime,
      'iss'           => $this->configuration['client_id'],
      'aud'           => $this->configuration['idp_eori'],
      'exp'           => $currentTime + 30,
    ];

    $jwtToken = FirebaseJWT::encode($body, $privateKey, self::ALGORITHM, head: $head);

    return [
      'query' => [
        'client_id'     => $this->configuration['client_id'],
        'response_type' => 'code',
        'scope'         => 'openid+ishare',
        'request'       => $jwtToken,
        'state'         => $this->stateToken->create(),
      ],
    ];
  }

  /**
   * Gets the client certificate chain and formats it as a x5c array.
   *
   * @return array The formatted x5c array
   */
  private function getX5CArray(): array
  {
    $x5c = $this->configuration['client_certificate_chain'];
    $x5c = str_replace("\r", '', $x5c);
    $x5c = str_replace("\n", '', $x5c);
    /** @var string $x5c */
    $x5c = str_replace('-----END CERTIFICATE-----', '', $x5c);
    $x5c = explode('-----BEGIN CERTIFICATE-----', $x5c);

    return array_values(array_filter($x5c));
  }
}
