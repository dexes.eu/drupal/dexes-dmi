<?php

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_ishare\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dexes_ishare\DexesIShare;
use Drupal\dexes_ishare\ManagedSettingsTrait;

class AdminSettingsForm extends ConfigFormBase
{
  use ManagedSettingsTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'dexes_ishare_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    if ($this->isManaged(DexesIShare::SETTINGS_KEY, TRUE)) {
      return $form;
    }

    $form['ishare'] = [
      '#type'        => 'details',
      '#title'       => $this->t('iSHARE settings'),
      '#description' => $this->t('General iSHARE settings.'),
      '#open'        => TRUE,

      'client_id' => [
        '#type'          => 'textfield',
        '#title'         => $this->t('Client EORI'),
        '#description'   => $this->t('Provide client EORI.'),
        '#default_value' => $this->config(DexesIShare::SETTINGS_KEY)->get('client_id'),
        '#required'      => TRUE,
      ],
      'private_key' => [
        '#type'          => 'textarea',
        '#title'         => $this->t('Private KEY'),
        '#description'   => $this->t('Provide private key.'),
        '#default_value' => $this->config(DexesIShare::SETTINGS_KEY)->get('private_key'),
        '#required'      => TRUE,
      ],
      'certificate_chain' => [
        '#type'          => 'textarea',
        '#title'         => $this->t('Client Certificate Chain'),
        '#description'   => $this->t('Client Certificate Chain'),
        '#default_value' => $this->config(DexesIShare::SETTINGS_KEY)->get('certificate_chain'),
        '#required'      => TRUE,
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    if ($this->isManaged(DexesIShare::SETTINGS_KEY, TRUE)) {
      return;
    }

    $config = $this->config(DexesIShare::SETTINGS_KEY);
    $config->set('client_id', $form_state->getValue('client_id'));
    $config->set('private_key', $form_state->getValue('private_key'));
    $config->set('certificate_chain', $form_state->getValue('certificate_chain'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return [DexesIShare::SETTINGS_KEY];
  }
}
