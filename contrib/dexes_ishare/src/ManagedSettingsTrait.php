<?php

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_ishare;

use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

trait ManagedSettingsTrait
{
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Determine if the settings by the given key are managed via the `settings.php` file.
   *
   * @return bool Whether the settings are managed
   */
  private function isManaged(string $settingsName, bool $addMessage = FALSE): bool
  {
    global $config;

    $isManaged = !empty($config[$settingsName]['is_managed']);

    if ($isManaged && $addMessage) {
      $this->messenger()
        ->addError($this->t('These settings are managed by your hosting provider. Contact an administrator to have them changed.'));
    }

    return $isManaged;
  }
}
