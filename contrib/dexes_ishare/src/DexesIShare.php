<?php

declare(strict_types=1);

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_ishare;

class DexesIShare
{
  public const SETTINGS_KEY = 'dexes_ishare.settings';

  public const ALGORITHM = 'RS256';
}
