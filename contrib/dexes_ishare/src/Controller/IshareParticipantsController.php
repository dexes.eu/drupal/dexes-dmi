<?php

declare(strict_types=1);

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_ishare\Controller;

use Dexes\CatalogSdk\CatalogSdk;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class IshareParticipantsController.
 */
class IshareParticipantsController extends ControllerBase
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self
  {
    /** @var CatalogSdk $catalogSdk */
    $catalogSdk = $container->get('catalog_sdk.sdk');

    /** @var LoggerChannelFactoryInterface $loggerFactory */
    $loggerFactory = $container->get('logger.factory');

    /** @var ConfigFactoryInterface $configFactory */
    $configFactory = $container->get('config.factory');

    /** @var bool $showParticipants */
    $showParticipants = $configFactory->get('dexes_dataspace.settings')->get('show_participants') ?? FALSE;

    return new self($catalogSdk, $loggerFactory->get('dexes_dataspace'), $showParticipants);
  }

  /**
   * IshareParticipantsController Constructor.
   *
   * @param CatalogSdk             $catalogSdk       The catalog sdk for retrieving the participants
   * @param LoggerChannelInterface $loggerChannel    A logger channel for logging exceptions
   * @param bool                   $showParticipants A setting which determines if the page return 404 or is rendered
   */
  public function __construct(private readonly CatalogSdk $catalogSdk,
                              private readonly LoggerChannelInterface $loggerChannel,
                              private readonly bool $showParticipants)
  {
  }

  /**
   * Returns a render array that displays a list of iSHARE participants.
   *
   * @return array<string, mixed> The Drupal render array
   */
  public function list(): array
  {
    if (!$this->showParticipants) {
      throw new NotFoundHttpException();
    }

    try {
      $parties = $this->catalogSdk->iShare()->parties(TRUE);
    } catch (ClientException|ResponseException $e) {
      $this->loggerChannel->error($e->getMessage() . ' | ' . $e->getPrevious()?->getMessage());

      $parties = [];
    }

    return [
      '#theme'   => 'ishare_participants_list',
      '#parties' => $parties,
      // We add daily refreshing cache here such that the list you
      // see when you go to the list endpoint will not lag behind
      // the actual list in the iSHARE satellite for too long.
      '#cache'   => [
        'keys'     => [
          'ishare-participants',
        ],
        'contexts' => ['user'],
        'tags'     => [
          'ishare-participants',
        ],
        'max-age'  => 86400,
      ],
    ];
  }

  /**
   * Returns a render array that displays a iSHARE participant.
   *
   * @return array<string, mixed> The Drupal render array
   */
  public function party(string $eori): array
  {
    if (!$this->showParticipants) {
      throw new NotFoundHttpException();
    }

    try {
      $party = $this->catalogSdk->iShare()->party($eori);
    } catch (ClientException|ResponseException $e) {
      $this->loggerChannel->error($e->getMessage() . ' | ' . $e->getPrevious()?->getMessage());

      $party = [];
    }

    return [
      '#theme'   => 'ishare_participant_view',
      '#party'   => $party,
      '#cache'   => [
        'keys'     => [
          'ishare-participants',
          'ishare-participant:' . $eori,
        ],
        'contexts' => ['user'],
        'tags'     => [
          'ishare-participants',
          'ishare-participant:' . $eori,
        ],

        'max-age'  => 86400,
      ],
    ];
  }
}
