<?php

declare(strict_types=1);

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_pdp\Factory;

use Dexes\ControlPlaneSdk\HttpRequestService;
use Dexes\ControlPlaneSdk\PDP\Repositories\AuthRepository;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\dexes_dmi\GenerateX5cArrayTrait;
use Drupal\dexes_ishare\DexesIShare;
use Drupal\dexes_pdp\DexesPDP;

/**
 * Class AuthRepositoryFactory.
 *
 * A factory for creating a HttpRequestService.
 */
final class AuthRepositoryFactory
{
  use GenerateX5cArrayTrait;

  /**
   * Creates a AuthRepository instance.
   *
   * @param ConfigFactoryInterface $configFactory The factory for accessing the config
   *
   * @return AuthRepository The created service
   */
  public static function create(HttpRequestService $requestService, LoggerChannelFactoryInterface $loggerFactory, ConfigFactoryInterface $configFactory): AuthRepository
  {
    $ishareConfig   = $configFactory->get(DexesIShare::SETTINGS_KEY);
    $pdpConfig      = $configFactory->get(DexesPDP::SETTINGS_KEY);

    $x5c = $ishareConfig->get('certificate_chain');

    return new AuthRepository(
      $requestService,
      $loggerFactory->get(DexesPDP::LOG_CHANNEL),
      self::getX5CArray($x5c),
      $pdpConfig->get('pdp_eori'),
      $ishareConfig->get('client_id'),
      $ishareConfig->get('private_key')
    );
  }
}
