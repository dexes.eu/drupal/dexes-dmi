<?php

declare(strict_types=1);

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_pdp\Factory;

use Dexes\ControlPlaneSdk\HttpRequestService;
use Dexes\ControlPlaneSdk\PDP\PdpClient;
use Dexes\ControlPlaneSdk\PDP\Repositories\AuthRepository;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\dexes_clearing\Services\CacheAdapter;
use Drupal\dexes_pdp\DexesPDP;

/**
 * Class PdpClientFactory.
 *
 * A factory for creating a PdpClient.
 */
final class PdpClientFactory
{
  /**
   * Creates a PdpClient instance.
   *
   * @param HttpRequestService            $requestService HttpRequestService $requestService the service used for making HTTP requests
   * @param AuthRepository                $authRepository The repository used for authentication purposes
   * @param LoggerChannelFactoryInterface $loggingFactory The logger factory used for creating loggers
   * @param CacheBackendInterface         $cache          The cache interface for caching data
   *
   * @return PdpClient The created service
   */
  public static function create(HttpRequestService $requestService,
                                AuthRepository $authRepository,
                                LoggerChannelFactoryInterface $loggingFactory,
                                CacheBackendInterface $cache): PdpClient
  {
    return new PdpClient(
      $requestService,
      $authRepository,
      $loggingFactory->get(DexesPDP::LOG_CHANNEL),
      new CacheAdapter($cache)
    );
  }
}
