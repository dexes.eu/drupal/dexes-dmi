<?php

declare(strict_types=1);

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_pdp\Factory;

use Dexes\ControlPlaneSdk\HttpRequestService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\dexes_pdp\DexesPDP;
use GuzzleHttp\Psr7\HttpFactory;

/**
 * Class HttpRequestServiceFactory.
 *
 * A factory for creating a HttpRequestService.
 */
final class HttpRequestServiceFactory
{
  /**
   * Creates a HttpRequestService instance.
   *
   * @param ConfigFactoryInterface $configFactory The factory for accessing the config
   * @param ClientFactory          $clientFactory The factory for creating configuration objects
   *
   * @return HttpRequestService The created service
   */
  public static function create(ConfigFactoryInterface $configFactory, ClientFactory $clientFactory): HttpRequestService
  {
    $config      = $configFactory->get(DexesPDP::SETTINGS_KEY);
    $httpFactory = new HttpFactory();

    return new HttpRequestService(
      $config->get('endpoint'),
      $clientFactory->fromOptions(),
      $httpFactory,
      $httpFactory,
    );
  }
}
