<?php

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_pdp\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dexes_ishare\ManagedSettingsTrait;
use Drupal\dexes_pdp\DexesPDP;

class PdpAdminSettingsForm extends ConfigFormBase
{
  use ManagedSettingsTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'dexes_pdp_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    if ($this->isManaged(DexesPDP::SETTINGS_KEY, TRUE)) {
      return $form;
    }

    $config = $this->config(DexesPDP::SETTINGS_KEY);

    $form['pdp'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Dexes PDP'),
      '#description' => $this->t('Dexes PDP settings.'),
      '#open'        => TRUE,

      'endpoint' => [
        '#type'          => 'textfield',
        '#title'         => $this->t('PDP endpoint'),
        '#description'   => $this->t('PDP endpoint'),
        '#default_value' => $config->get('endpoint'),
        '#required'      => TRUE,
      ],

      'pdp_eori' => [
        '#type'          => 'textfield',
        '#title'         => $this->t('PDP Eori'),
        '#description'   => $this->t('PDP Eori'),
        '#default_value' => $config->get('pdp_eori'),
        '#required'      => TRUE,
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    if ($this->isManaged(DexesPDP::SETTINGS_KEY, TRUE)) {
      return;
    }

    $formFields = [
      'endpoint',
      'pdp_eori',
    ];

    $config = $this->config(DexesPDP::SETTINGS_KEY);

    foreach ($formFields as $field) {
      $config->set($field, $form_state->getValue($field));
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return [DexesPDP::SETTINGS_KEY];
  }
}
