<?php

declare(strict_types=1);

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_pdp;

/**
 * Class DexesPDP.
 *
 * Simple PHP class holding various constants used by the `DexesPDP` Drupal module.
 */
final class DexesPDP
{
  /**
   * The key identifying the `dexes_pdp` settings in Drupal.
   *
   * @var string
   */
  public const SETTINGS_KEY = 'dexes_pdp.settings';

  /**
   * The log channel that should be used by this Drupal module.
   *
   * @var string
   */
  public const LOG_CHANNEL = 'dexes_pdp';
}
