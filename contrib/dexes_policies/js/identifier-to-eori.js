/* globals jQuery, Drupal, drupalSettings */

(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.identifierToEori = {
        attach: function () {
            "use strict";

            $(".identifier-to-eori").each((index, currentElement) => {
                const element = $(currentElement);
                const targetFieldName = element.attr("data-target-field-name");
                const targetElement = $(targetFieldName);
                if (!drupalSettings.dexes_policies.identifierToEORI) {
                    return;
                }
                const identifierToEORI = JSON.parse(drupalSettings.dexes_policies.identifierToEORI);

                targetElement.on("change", () => {
                    const newIdentifier = targetElement.val();

                    const eori = identifierToEORI[newIdentifier];

                    if (eori !== undefined) {
                        element.val(eori);
                    }
                });
            });
        }
    };
})(jQuery, Drupal, drupalSettings);
