<?php

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_policies\Services;

use Dexes\ClearingSdk\ClearingSdk;
use Psr\SimpleCache\InvalidArgumentException;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

trait PolicyDetailsTrait
{
  private const COMMON_POLICY_LANGUAGE = 'AMdEX';  // If a policy template has this value as policy_language, it is a common policy template

  /**
   * Converts the identifier of an offer policy into the actual policy.
   *
   * @param array       $dataset     The dataset to convert the policies off
   * @param ClearingSdk $clearingSdk The clearing sdk for interacting with policies
   */
  private function convertDatasetPolicies(array &$dataset, ClearingSdk $clearingSdk): void
  {
    if (isset($dataset['attributes']['odrl:hasPolicy'][0])) {
      $dataset['attributes']['odrl:hasPolicy'] = $this->getOfferPolicy($dataset['attributes']['odrl:hasPolicy'][0], $clearingSdk);
    }
    foreach ($dataset['relationships'] as &$resource) {
      if (isset($resource['data']['attributes']['odrl:hasPolicy'][0])) {
        $resource['data']['attributes']['odrl:hasPolicy'] = $this->getOfferPolicy($resource['data']['attributes']['odrl:hasPolicy'][0], $clearingSdk);
      }
    }

    $this->addDatasetPolicyValidationRequirements($dataset);
  }

  /**
   * Converts the identifier of an offer policy into the actual policy.
   *
   * @param array       $dataService The dataset to convert the policies off
   * @param ClearingSdk $clearingSdk The clearing sdk for interacting with policies
   */
  private function convertDataServicePolicies(array &$dataService, ClearingSdk $clearingSdk): void
  {
    if (isset($dataService['attributes']['odrl:hasPolicy'][0])) {
      $dataService['attributes']['odrl:hasPolicy'] = $this->getOfferPolicy($dataService['attributes']['odrl:hasPolicy'][0], $clearingSdk);
    }

    $this->addDataservicePolicyValidationRequirements($dataService);
  }

  /**
   * Converts the identifier of an offer policy into the actual policy.
   *
   * @param string      $policyId    The identifier of the offer policy
   * @param ClearingSdk $clearingSdk The clearing sdk for interacting with policies
   *
   * @return array<string, mixed> The offer policy
   */
  private function getOfferPolicy(string $policyId, ClearingSdk $clearingSdk): array
  {
    try {
      $policy = $clearingSdk->dataspaceOfferPolicy()->get($policyId);

      foreach ($policy['policy_templates'] as &$policyTemplate) {
        $policyTemplate['authority']['authority_logo'] = $this->convertAuthorityToIcon($policyTemplate['authority']['authority_id']);
        $policyTemplate['publisher']['publisher_logo'] = $this->convertAuthorityToIcon($policyTemplate['publisher']['publisher_id']);
      }

      return $policy;
    } catch (InvalidArgumentException|ClientException|ResponseException $e) {
      return [];
    }
  }

  /**
   * Check for a policy whether it is a common policy.
   *
   * @param array<string, mixed> $policy The policy which we want to check for common status
   *
   * @return bool True if the given policy is a common policy, false otherwise
   */
  private function isCommonPolicy(array $policy): bool
  {
    foreach ($policy['policy_templates'] as $policyTemplate) {
      $policyTemplateLanguage = $policyTemplate['policy']['policy_language'] ?? NULL;
      if (self::COMMON_POLICY_LANGUAGE === $policyTemplateLanguage) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Adds the validation requirements for a dataservice.
   *
   * @param array $dataservice The dataservice to convert
   */
  private function addDataservicePolicyValidationRequirements(array &$dataservice): void
  {
    if (empty($dataservice['attributes']['odrl:hasPolicy'])) {
      return;
    }

    $dataservice['attributes']['odrl:hasPolicy']['policy_validation_rules'] = [];

    foreach ($dataservice['attributes']['odrl:hasPolicy']['policy_templates'] as $policy) {
      if (is_null($policy)) {
        continue;
      }

      $this->getValidationRequirements($dataservice['attributes']['odrl:hasPolicy']['policy_validation_rules'], $policy);
    }
  }

  /**
   * Adds the validation requirements for each resource without duplicate requirements.
   *
   * @param array $dataset The dataset to convert
   */
  private function addDatasetPolicyValidationRequirements(array &$dataset): void
  {
    $datasetPolicyValidationRequirements = [];

    if (!empty($dataset['attributes']['odrl:hasPolicy'])) {
      foreach ($dataset['attributes']['odrl:hasPolicy']['policy_templates'] as $policy) {
        if (is_null($policy)) {
          continue;
        }

        $this->getValidationRequirements($datasetPolicyValidationRequirements, $policy);
      }
    }

    foreach ($dataset['relationships'] as &$resource) {
      $resource['data']['attributes']['odrl:hasPolicy']['policy_validation_rules'] = $datasetPolicyValidationRequirements;

      if (empty($resource['data']['attributes']['odrl:hasPolicy'])) {
        continue;
      }
      foreach ($resource['data']['attributes']['odrl:hasPolicy']['policy_templates'] as $policy) {
        if (is_null($policy)) {
          continue;
        }

        $this->getValidationRequirements($resource['data']['attributes']['odrl:hasPolicy']['policy_validation_rules'], $policy);
      }
    }
  }

  /**
   * Adds the validationRequirements to the given list, if the requirement is already there skip it.
   *
   * @param array      $policyValidationRequirements The list of requirements
   * @param null|array $policy                       The policy which contains the validation requirements
   */
  private function getValidationRequirements(array &$policyValidationRequirements, ?array $policy): void
  {
    if (is_null($policy)) {
      return;
    }

    foreach ($policy['policy']['required_facts'] as $validation) {
      if (in_array($validation['id'], $policyValidationRequirements)) {
        continue;
      }

      $validation['language']                          = $policy['policy']['policy_language'];
      $policyValidationRequirements[$validation['id']] = $validation;
    }
  }

  private function convertAuthorityToIcon(string $authorityId): string
  {
    return match ($authorityId) {
      'http://registry.dexes.eu/amdex' => '/themes/custom/dexes/assets/images/badges/amdex.svg',
      'http://registry.dexes.eu/dexes' => '/themes/custom/dexes/assets/images/badges/dexes.png',
      'http://registry.dexes.eu/dmi'   => '/themes/custom/dexes/assets/images/badges/dmi-small.png',
      default                          => '',
    };
  }
}
