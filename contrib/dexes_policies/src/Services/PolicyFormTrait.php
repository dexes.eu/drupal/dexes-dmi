<?php

declare(strict_types=1);

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_policies\Services;

use Drupal\Core\Form\FormStateInterface;
use Psr\SimpleCache\InvalidArgumentException;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

trait PolicyFormTrait
{
  /**
   * Add extra libraries to the form.
   *
   * @param array $form The form to add the libraries to
   */
  abstract protected function addExtraLibraries(array &$form): void;

  /**
   * Adds a policy field to the form.
   */
  protected function buildPolicyPartOfForm(FormStateInterface $form_state, array &$form, array &$formPart, array $policies = []): void
  {
    $selectedPolicies = $policies['template_policies'];
    $label            = $policies['label'];
    $entitledParty    = $policies['entitled_party'];
    $serviceProvider  = $policies['service_provider'];

    $policies = $this->getPolicyOptions();
    $count    = $form_state->get('policies.count');
    $lists    = [];

    foreach ($selectedPolicies as $key => $selectedPolicy) {
      if (is_array($selectedPolicy)) {
        $selectedPolicy = $selectedPolicy['policy_' . $key];
      }
      if (isset($policies[$selectedPolicy])) {
        $lists[] = $selectedPolicy;
      }
    }

    if (NULL === $count) {
      $count = count(array_keys($lists));
      $form_state->set('policies.count', $count);
    }

    $formPart['policy_templates']['has_policy'] = [
      '#type'   => 'container',
      '#tree'   => TRUE,
      '#prefix' => '
        <div id="policies-wrapper">
          <table>
            <thead>
            <tr>
              <th class="font-weight-normal pb-2">Policy</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
      ',
      '#suffix' => '
            </tbody>
          </table>
        </div>
      ',
    ];

    for ($i = 0; $i < $count; ++$i) {
      $formPart['policy_templates']['has_policy'][$i] = [
        'policy_' . $i => [
          '#type'          => 'select2',
          '#title'         => $this->t('Type')->render(),
          '#title_display' => 'invisible',
          '#default_value' => $lists[$i] ?? NULL,
          '#options'       => $policies,
          '#prefix'        => '<tr><td>',
          '#suffix'        => '</td>',
          '#empty_option'  => $this->t('- Select -'),
          '#attributes'    => [
            'class' => ['w-100', 'policies-multi-select'],
            'id'    => 'policy-select-' . $i,
          ],
        ],
        'static_policy_' . $i => [
          '#prefix' => '<td class="d-none static-policy-cell"><p>' . ($lists[$i] ?? ''),
          '#suffix' => '</p></td>',
        ],
        'delete_' . $i => [
          '#type'                    => 'submit',
          '#value'                   => $this->t('Delete')->render(),
          '#submit'                  => ['::deleteOne'],
          '#prefix'                  => '<td>',
          '#suffix'                  => '</td></tr>',
          '#limit_validation_errors' => [],
          '#attributes'              => [
            'class'             => ['btn', 'btn-danger', 'ml-2', 'delete-button', 'mb-1'],
            'data-delete-index' => $i,
          ],
          '#data' => $i,
          '#ajax' => [
            'callback' => '::alterMoreCallback',
            'wrapper'  => 'policies-wrapper',
            'event'    => 'click',
          ],
        ],
      ];
    }

    if (0 === $count) {
      $formPart['policy_templates']['has_policy']['message'] = [
        '#type'   => 'container',
        '#prefix' => '<tr><td class="font-italic">' . t('No policies are applicable'),
        '#suffix' => '</td></tr>',
      ];
    }

    $formPart['policy_templates']['row']['delete_index'] = [
      '#type'       => 'hidden',
      '#attributes' => [
        'id' => 'delete_index',
      ],
    ];

    $formPart['policy_templates']['row']['#type'] = 'actions';
    $formPart['policy_templates']['row']['add']   = [
      '#type'                    => 'submit',
      '#value'                   => $this->t('Add'),
      '#submit'                  => ['::addOne'],
      '#limit_validation_errors' => [],
      '#attributes'              => [
        'class' => ['btn', 'btn-success', 'add-button', 'ml-0'],
      ],
      '#ajax' => [
        'callback' => '::alterMoreCallback',
        'wrapper'  => 'policies-wrapper',
        'event'    => 'click',
      ],
    ];

    $this->addOfferPolicyFields($formPart, $label, $entitledParty, $serviceProvider);

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $form['#attached']['library'][] = 'dexes_policies/identifier-to-eori';

    $list = array_map(function($value) {
      return $value['EORI'];
    }, $this->listClient->list('iShare:Organization')->getData());
    $form['#attached']['drupalSettings']['dexes_policies']['identifierToEORI'] = json_encode($list);

    $this->addExtraLibraries($form);
  }

  /**
   * Gets the selected policies from the correct source.
   *
   * @param FormStateInterface $form_state The current state of the form
   * @param array              $data       The saved data
   *
   * @return array|string[] The selected policies
   */
  protected function getSelectedPolicies(FormStateInterface $form_state, array $data): array
  {
    $userData         = $form_state->getUserInput();
    $templatePolicies = [];
    $offerPolicy      = $data['attributes']['odrl:hasPolicy'] ?? NULL;
    $label            = [];
    $entitledParty    = NULL;
    $serviceProvider  = NULL;

    if (!empty($userData['has_policy'])) {
      $templatePolicies = $userData['has_policy'];
      $label            = [
        'en-US' => $userData['policy_label_en'] ?? '',
        'nl-NL' => $userData['policy_label_nl'] ?? '',
      ];
      $entitledParty    = $userData['entitled_party']   ?? NULL;
      $serviceProvider  = $userData['service_provider'] ?? NULL;

      $templatePolicies = is_string($templatePolicies) ? [$templatePolicies] : $templatePolicies;
    } elseif (isset($offerPolicy[0])) {
      try {
        $response = $this->clearingSdk->dataspaceOfferPolicy()->get($offerPolicy[0]);

        $templatePolicies = array_map(function($policyTemplate) {
          return $policyTemplate['id'];
        }, $response['policy_templates']);
        $label            = $response['label'];
        $entitledParty    = $response['entitled_party'];
        $serviceProvider  = $response['service_provider'];
      } catch (InvalidArgumentException|ClientException|ResponseException $e) {
      }
    }

    return [
      'template_policies' => $templatePolicies,
      'label'             => $label,
      'entitled_party'    => $entitledParty   ?? $this->userEori,
      'service_provider'  => $serviceProvider ?? $this->userEori,
    ];
  }

  /**
   * Normalizes the form values for policies.
   *
   * @param array $formData The form data
   *
   * @return bool returns true on success and false on a failure
   */
  protected function normalizePolicies(array &$formData, array|null $oldPolicies, string|null $targetUrl): bool
  {
    $policies = [];
    if (!isset($formData['has_policy'])) {
      $formData['has_policy'] = [];

      return TRUE;
    }

    foreach ($formData['has_policy'] as $index => $policy) {
      $policies[] = $policy['policy_' . $index];
    }

    $label = [
      'en-US' => $formData['policy_label_en'] ?? '',
      'nl-NL' => $formData['policy_label_nl'] ?? '',
    ];

    $entitledParty    = $formData['entitled_party']   ?? '';
    $serviceProvider  = $formData['service_provider'] ?? '';

    unset($formData['policy_label_en'], $formData['policy_label_nl'], $formData['entitled_party'], $formData['service_provider']);

    try {
      if (!is_null($oldPolicies) && !empty($oldPolicies[0])) {
        $response = $this->clearingSdk->dataspaceOfferPolicy()->get($oldPolicies[0]);
        $equal    = FALSE;

        if ($response['label'] === $label && $response['entitled_party'] === $entitledParty && $response['service_provider'] === $serviceProvider && sizeof($policies) === sizeof($response['policy_templates'])) {
          $equal = TRUE;
          for ($i = 0; $i < sizeof($policies); ++$i) {
            if ($policies[$i] !== $response['policy_templates'][$i]['id']) {
              $equal = FALSE;
            }
          }
        }

        if ($equal) {
          $formData['has_policy'] = [$response['id']];

          return TRUE;
        }

        $this->clearingSdk->dataspaceOfferPolicy()->delete($oldPolicies[0]);
      }
    } catch (InvalidArgumentException|ClientException|ResponseException) {
      $this->logger('dexes_policies')->error('Could not retrieve policy offer with id: ' . $oldPolicies[0]);
    }
    if (is_null($targetUrl)) {
      $formData['has_policy'] = [
        'label'            => $label,
        'policies'         => $policies,
        'entitled_party'   => $entitledParty,
        'service_provider' => $serviceProvider,
      ];

      return TRUE;
    }

    try {
      $response = $this->clearingSdk->dataspaceOfferPolicy()->post(
        $policies,
        $targetUrl,
        $entitledParty,
        $serviceProvider,
        $label
      );
    } catch (ClientException|ResponseException $e) {
      $message = $e->getMessage();

      if ($e instanceof ResponseException && $e->response->hasJson()) {
        $errors = $e->response->json(TRUE);
        $errors = $errors['errors'] ?? [];
        $message .= ' ' . json_encode($errors);
      }

      $this->messenger()->addError(t('A system error prevented the creation of the Policy.'));
      $this->logger('dexes_policies')->error('Could not create offer policy: ' . $message);
      $formData['has_policy'] = [];

      return FALSE;
    }

    $formData['has_policy'] = [$response['uuid']];

    return TRUE;
  }

  /**
   * Adds extra policy fields to the form part.
   *
   * @param array        $formPart        The form part to add the field to
   * @param array|string $label           The label of the policy
   * @param string       $entitledParty   The entitled party of the policy
   * @param string       $serviceProvider The service provider of the policy
   */
  private function addOfferPolicyFields(array &$formPart, array|string $label, string $entitledParty, string $serviceProvider): void
  {
    $formPart['policy_label_en'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('The name of the policy in english'),
      '#default_value' => $label['en-US'] ?? $label,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#states'        => [
        'required' => [
          ':input[name="has_policy[0][policy_0]"]' => [
            'filled' => TRUE,
          ],
        ],
      ],
    ];

    $formPart['policy_label_nl'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('The name of the policy in dutch'),
      '#default_value' => $label['nl-NL'] ?? $label,
      '#attributes'    => [
        'class' => ['form-control'],
      ],
      '#states'        => [
        'required' => [
          ':input[name="has_policy[0][policy_0]"]' => [
            'filled' => TRUE,
          ],
        ],
      ],
    ];

    $formPart['entitled_party'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Entitled Party'),
      '#description'   => $this->t('EORI of the entitled party who owns the asset'),
      '#default_value' => $entitledParty,
      '#attributes'    => [
        'class'                  => ['form-control', 'identifier-to-eori'],
        'data-target-field-name' => '.entitled-party-target',
      ],
      '#element_validate' => [
        [static::class, 'validateEORIToken'],
      ],
      '#states'        => [
        'required' => [
          ':input[name="has_policy[0][policy_0]"]' => [
            'filled' => TRUE,
          ],
        ],
      ],
    ];

    $formPart['service_provider'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Service Provider'),
      '#description'   => $this->t('EORI of the service provider that provides the asset'),
      '#default_value' => $serviceProvider,
      '#attributes'    => [
        'class'                  => ['form-control', 'identifier-to-eori'],
        'data-target-field-name' => '.service-provider-target',
      ],
      '#element_validate' => [
        [static::class, 'validateEORIToken'],
      ],
      '#states'        => [
        'required' => [
          ':input[name="has_policy[0][policy_0]"]' => [
            'filled' => TRUE,
          ],
        ],
      ],
    ];
  }

  /**
   * Formats the policy list data to key value pairs for a form.
   */
  private function getPolicyOptions(): array
  {
    $list = $this->listClient->list('DEXES:Policies');

    try {
      $policies = $this->clearingSdk->dataspacePolicyTemplate()->index();
    } catch (ClientException $e) {
      $this->logger('clearingSDK')->error('Could not retrieve policy templates: ' . $e->getMessage());

      return [];
    }

    $options = [];

    foreach ($policies as $policy) {
      $options[$policy['id']] = $policy['policy_info']['policy_label'][$list->getLanguageCode()];
    }

    return $options;
  }
}
