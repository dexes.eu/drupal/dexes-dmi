<?php

declare(strict_types=1);

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_policies\TwigExtension;

use Drupal\Core\Language\LanguageManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\ExtensionInterface;
use Twig\TwigFilter;

/**
 * Class PolicyTwigExtension.
 *
 * Contains several Twig filter functions for policies.
 */
class PolicyTwigExtension extends AbstractExtension implements ExtensionInterface
{
  /**
   * Instantiates a new instance of this class.
   */
  public static function create(LanguageManagerInterface $languageManager): PolicyTwigExtension
  {
    $language = $languageManager->getCurrentLanguage()->getId();

    return new self('en' === $language ? 'en-US' : 'nl-NL');
  }

  /**
   * TranslatableListTwigExtension constructor.
   *
   * @param string $language The current language of the site
   */
  public function __construct(private readonly string $language)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters(): array
  {
    return [
      new TwigFilter('translate_policy_value', [$this, 'translatePolicyValue']),
    ];
  }

  /**
   * Gets The data for an identifier based on the list its in and the
   * current language.
   *
   * @param null|array<string, mixed> $policyValue a value of the policy  that needs to be translated
   *
   * @return string the label of the policy
   */
  public function translatePolicyValue(?array $policyValue): string
  {
    if (is_null($policyValue)) {
      return '';
    }

    return $policyValue[$this->language] ?? '';
  }
}
