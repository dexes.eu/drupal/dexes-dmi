<?php

declare(strict_types=1);

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_clearing\Form;

use Dexes\ClearingSdk\ClearingSdk;
use Dexes\ClearingSdk\Repositories\ClearingService\ClearingRepository;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\dexes_clearing\DexesClearing;
use Drupal\dexes_ishare\DexesIShare;
use Drupal\user\Entity\User;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class InitiateServiceClearingForm.
 */
final class InitiateServiceClearingForm extends FormBase
{
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): InitiateServiceClearingForm
  {
    /** @var ClearingSdk $clearingSdk */
    $clearingSdk = $container->get('dexes_clearing.h2msdk');

    /** @var AccountProxyInterface $user */
    $user = $container->get('current_user');

    $user = User::load($user->id());

    /** @var ConfigFactoryInterface $configFactory */
    $configFactory = $container->get('config.factory');
    $config        = $configFactory->get(DexesClearing::SETTINGS_KEY);
    $ishareConfig  = $configFactory->get(DexesIShare::SETTINGS_KEY);

    /** @var LoggerChannelFactoryInterface $loggerFactory */
    $loggerFactory = $container->get('logger.factory');

    return new self(
      $clearingSdk->clearing(),
      $user->field_company_id?->value  ?? NULL,
      $user->field_idp_user_id?->value ?? NULL,
      $config->get('clearing_callback_endpoint'),
      $ishareConfig->get('client_id'),
      $config->get('clearing_request_detail_url_prefix'),
      $loggerFactory->get(DexesClearing::LOGGER_CHANNEL)
    );
  }

  /**
   * InitiateServiceClearingForm Constructor.
   *
   * @param ClearingRepository     $clearingRepository  The clearing repository
   * @param null|string            $companyEori         The company eori of the user
   * @param null|string            $userId              The id of the user in the idp
   * @param string                 $callbackEndpoint    The endpoint that is called when the request is completed in clearing
   * @param string                 $machineEori         The eori of the dataspace
   * @param string                 $viewRequestEndpoint The endpoint to view a clearing request
   * @param LoggerChannelInterface $logger              A logger channel for logging exceptions
   */
  public function __construct(private readonly ClearingRepository $clearingRepository,
                              private readonly ?string $companyEori,
                              private readonly ?string $userId,
                              private readonly string $callbackEndpoint,
                              private readonly string $machineEori,
                              private readonly string $viewRequestEndpoint,
                              private readonly LoggerChannelInterface $logger)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'dexes_clearing_service_initiate_clearing_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?array $service = NULL): array
  {
    if (is_null($service)) {
      return [];
    }

    $form['submit'] = [
      '#type'       => 'submit',
      '#value'      => $this->t('Request service'),
      '#attributes' => [
        'class' => [
          'btn',
          'btn-primary',
          'ml-0',
          'mb-3',
        ],
      ],
    ];

    if (!$this->canCreateClearingRequest($service)) {
      $form['submit']['#attributes']['disabled'] = [
        'disabled' => TRUE,
      ];
      $form['submit']['#attributes']['title'] = $this->t('Log in with your iShare account to request this service.');

      $form['message'] = [
        '#type'  => 'html_tag',
        '#tag'   => 'p',
        '#value' => $this->t('Log in with your iShare account to request this service.'),
      ];
    } elseif ($this->hasActiveClearingRequest($service['meta']['name'])) {
      $form['submit']['#attributes']['disabled'] = [
        'disabled' => TRUE,
      ];

      $form['submit']['#attributes']['title'] = $this->t('You already have requested this service.');

      $form['message'] = [
        '#type'  => 'html_tag',
        '#tag'   => 'p',
        '#value' => $this->t('You already have requested this service.'),
      ];
    }

    $form['#attributes']['class'] = [
      'mt-4',
    ];

    $form['#attributes']['target'] = ['_blank'];

    $form['#cache']['keys'][] = 'user:' . $this->userId;
    $form['#cache']['tags'][] = 'user:' . $this->userId;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $service = $this->getRouteMatch()->getParameter('service');

    if (!$this->canCreateClearingRequest($service) && $this->hasActiveClearingRequest($service['meta']['name'])) {
      throw new NotFoundHttpException();
    }

    try {
      $response = $this->clearingRepository->start(
        $service['attributes']['odrl:hasPolicy'][0],
        $this->userId      ?? '',
        $this->companyEori ?? '',
        [
          'url'      => sprintf('%s?type=service&id=%s', $this->callbackEndpoint, $service['meta']['name']),
          'audience' => $this->machineEori,
        ]
      );

      $url = sprintf('%s/%s', $this->viewRequestEndpoint, $response['clearing_instance_id']);

      $form_state->setResponse(new TrustedRedirectResponse($url, 302));
    } catch (InvalidArgumentException|ClientException|ResponseException $e) {
      $this->logger->error(sprintf('Could not start clearing for user "%s": %s', $this->userId, $e->getMessage()));

      $this->messenger()->addError(t('Could not start a clearing request, please try again later.'));
    }
  }

  /**
   * Checks if the current user is allowed to create a clearing request.
   *
   * @return bool Returns true if the current user is allowed to create a clearing request
   */
  private function canCreateClearingRequest(array $service): bool
  {
    return !(is_null($this->userId) || is_null($this->companyEori) || empty($service) || empty($service['attributes']['odrl:hasPolicy']));
  }

  /**
   * Checks if the current user already has a clearing request.
   *
   * @param string $serviceName The name of the service
   *
   * @return bool Returns true if the current user already has a clearing request
   */
  private function hasActiveClearingRequest(string $serviceName): bool
  {
    if (is_null($this->companyEori)) {
      return FALSE;
    }

    try {
      $requests = $this->clearingRepository->requester($this->companyEori);
    } catch (InvalidArgumentException|ClientException|ResponseException $e) {
      $this->logger->error(sprintf('Could not retrieve clearing instances with company eori "%s": %s', $this->companyEori, $e->getMessage()));

      return FALSE;
    }

    $requests = array_filter($requests, function(array $clearingInstance) use ($serviceName) {
      return in_array($clearingInstance['status'], ['waiting', 'approved']) && $clearingInstance['requester_user']         === $this->userId
                                                                            && $clearingInstance['offer_policy']['target'] === $serviceName;
    });

    return !empty($requests);
  }
}
