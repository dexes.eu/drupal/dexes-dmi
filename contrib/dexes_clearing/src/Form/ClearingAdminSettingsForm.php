<?php

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_clearing\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dexes_clearing\DexesClearing;
use Drupal\dexes_ishare\ManagedSettingsTrait;

class ClearingAdminSettingsForm extends ConfigFormBase
{
  use ManagedSettingsTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'dexes_ishare_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    if ($this->isManaged(DexesClearing::SETTINGS_KEY, TRUE)) {
      return $form;
    }

    $config = $this->config(DexesClearing::SETTINGS_KEY);

    $form['clearing'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Dexes Clearing'),
      '#description' => $this->t('Dexes Clearing settings.'),
      '#open'        => TRUE,

      'clearing_enabled' => [
        '#type'          => 'checkbox',
        '#title'         => $this->t('Clearing enabled'),
        '#default_value' => $config->get('clearing_enabled'),
      ],
      'clearing_endpoint' => [
        '#type'          => 'textfield',
        '#title'         => $this->t('Clearing endpoint'),
        '#description'   => $this->t('Clearing endpoint'),
        '#default_value' => $config->get('clearing_endpoint'),
        '#required'      => TRUE,
      ],
      'clearing_request_detail_url_prefix' => [
        '#type'          => 'textfield',
        '#title'         => $this->t('Clearing request detail url prefix (Entitled Party)'),
        '#description'   => $this->t('Clearing request detail url prefix (Entitled Party)'),
        '#default_value' => $config->get('clearing_request_detail_url_prefix'),
        '#required'      => TRUE,
      ],
      'clearing_request_detail_url_prefix_service_consumer' => [
        '#type'          => 'textfield',
        '#title'         => $this->t('Clearing request detail url prefix (Service Consumer)'),
        '#description'   => $this->t('Clearing request detail url prefix (Service Consumer)'),
        '#default_value' => $config->get('clearing_request_detail_url_prefix_service_consumer'),
        '#required'      => TRUE,
      ],
      'clearing_new_offer_policy_endpoint' => [
        '#type'          => 'textfield',
        '#title'         => $this->t('Clearing new offer policy endpoint'),
        '#description'   => $this->t('Clearing new offer policy endpoint'),
        '#default_value' => $config->get('clearing_new_offer_policy_endpoint'),
        '#required'      => TRUE,
      ],
      'clearing_view_offer_policy_endpoint' => [
        '#type'          => 'textfield',
        '#title'         => $this->t('Clearing view offer policy endpoint'),
        '#description'   => $this->t('Clearing view offer policy endpoint'),
        '#default_value' => $config->get('clearing_view_offer_policy_endpoint'),
        '#required'      => TRUE,
      ],
      'clearing_callback_endpoint' => [
        '#type'          => 'textfield',
        '#title'         => $this->t('Clearing callback endpoint'),
        '#description'   => $this->t('The endpoint is used to callback when clearing is successful'),
        '#default_value' => $config->get('clearing_callback_endpoint'),
        '#required'      => TRUE,
      ],
      'clearing_eori' => [
        '#type'          => 'textfield',
        '#title'         => $this->t('Clearing EORI'),
        '#description'   => $this->t('Clearing EORI.'),
        '#default_value' => $config->get('clearing_eori'),
        '#required'      => TRUE,
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    if ($this->isManaged(DexesClearing::SETTINGS_KEY, TRUE)) {
      return;
    }

    $formFields = [
      'clearing_enabled',
      'clearing_endpoint',
      'clearing_request_detail_url_prefix',
      'clearing_request_detail_url_prefix_service_consumer',
      'clearing_eori',
      'clearing_new_offer_policy_endpoint',
      'clearing_callback_endpoint',
      'clearing_view_offer_policy_endpoint',
    ];

    $config = $this->config(DexesClearing::SETTINGS_KEY);

    foreach ($formFields as $field) {
      $config->set($field, $form_state->getValue($field));
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array
  {
    return [DexesClearing::SETTINGS_KEY];
  }
}
