<?php

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_clearing\Factories;

use Dexes\ClearingSdk\ClearingSdk;
use Dexes\ClearingSdk\HttpRequestService;
use Dexes\ClearingSdk\Repositories\Auth\AuthRepository;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\dexes_clearing\DexesClearing;
use Drupal\dexes_clearing\Services\CacheAdapter;
use Drupal\dexes_clearing\Services\H2MClearingSdk;

class ClearingSdkFactory
{
  /**
   * Creates a new instance of ClearingSdk.
   *
   * @param HttpRequestService            $requestService The HTTP request service used for sending requests
   * @param AuthRepository                $authRepository The auth repository used for managing authentication
   * @param LoggerChannelFactoryInterface $loggerFactory  The logger factory used for creating loggers
   * @param CacheBackendInterface         $cache          The cache backend used for caching data
   *
   * @return ClearingSdk The created instance of ClearingSdk
   */
  public static function create(
    HttpRequestService $requestService,
    AuthRepository $authRepository,
    LoggerChannelFactoryInterface $loggerFactory,
    CacheBackendInterface $cache
  ): ClearingSdk {
    return new ClearingSdk(
      $requestService,
      $authRepository,
      $loggerFactory->get(DexesClearing::LOGGER_CHANNEL),
      new CacheAdapter($cache)
    );
  }

  /**
   * Creates a new instance of H2MClearingSdk.
   *
   * @param HttpRequestService            $requestService The HTTP request service used for sending requests
   * @param AuthRepository                $authRepository The auth repository used for managing authentication
   * @param LoggerChannelFactoryInterface $loggerFactory  The logger factory used for creating loggers
   * @param CacheBackendInterface         $cache          The cache backend used for caching data
   * @param AccountProxy                  $currentUser    The current user that will be using the H2MClearingSDK
   */
  public static function h2mCreate(
    HttpRequestService $requestService,
    AuthRepository $authRepository,
    LoggerChannelFactoryInterface $loggerFactory,
    CacheBackendInterface $cache,
    AccountProxy $currentUser,
  ): H2MClearingSdk {
    return new H2MClearingSdk(
      $requestService,
      $authRepository,
      $loggerFactory->get(DexesClearing::LOGGER_CHANNEL),
      new CacheAdapter($cache),
      (string) $currentUser->id()
    );
  }
}
