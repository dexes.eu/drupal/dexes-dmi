<?php

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_clearing\Factories;

use Dexes\ClearingSdk\HttpRequestService;
use Dexes\ClearingSdk\Repositories\Auth\AuthRepository;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\dexes_clearing\DexesClearing;
use Drupal\dexes_dmi\GenerateX5cArrayTrait;
use Drupal\dexes_ishare\DexesIShare;

class AuthRepositoryFactory
{
  use GenerateX5cArrayTrait;

  /**
   * Creates an instance of AuthRepository.
   *
   * @param HttpRequestService            $requestService       the HTTP request service
   * @param LoggerChannelFactoryInterface $loggerChannelFactory the logger channel factory
   * @param ConfigFactoryInterface        $configFactory        the configuration factory
   *
   * @return AuthRepository the created AuthRepository instance
   */
  public static function create(HttpRequestService $requestService,
                                LoggerChannelFactoryInterface $loggerChannelFactory,
                                ConfigFactoryInterface $configFactory): AuthRepository
  {
    $ishareConfig   = $configFactory->get(DexesIShare::SETTINGS_KEY);
    $clearingConfig = $configFactory->get(DexesClearing::SETTINGS_KEY);

    $x5c = $ishareConfig->get('certificate_chain');

    return new AuthRepository(
      $requestService,
      $loggerChannelFactory->get(DexesClearing::LOGGER_CHANNEL),
      self::getX5CArray($x5c),
      $clearingConfig->get('clearing_eori'),
      $ishareConfig->get('client_id'),
      $ishareConfig->get('private_key')
    );
  }
}
