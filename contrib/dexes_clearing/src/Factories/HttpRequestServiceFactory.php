<?php

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_clearing\Factories;

use Dexes\ClearingSdk\HttpRequestService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\dexes_clearing\DexesClearing;
use GuzzleHttp\Psr7\HttpFactory;

class HttpRequestServiceFactory
{
  /**
   * Create a new instance of HttpRequestService.
   *
   * @param ClientFactory          $clientFactory the factory for creating HTTP clients
   * @param ConfigFactoryInterface $configFactory the factory for creating configuration objects
   *
   * @return HttpRequestService the newly created HttpRequestService object
   */
  public static function create(ClientFactory $clientFactory,
                                ConfigFactoryInterface $configFactory): HttpRequestService
  {
    $httpFactory = new HttpFactory();

    return new HttpRequestService(
      (string) $configFactory->get(DexesClearing::SETTINGS_KEY)->get('clearing_endpoint'),
      $clientFactory->fromOptions(),
      $httpFactory,
      $httpFactory
    );
  }
}
