<?php

declare(strict_types=1);

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_clearing\EventSubscriber;

use Dexes\ClearingSdk\Exceptions\H2MApiKeyNotFoundInCacheException;
use Dexes\ClearingSdk\Repositories\Auth\AuthRepository;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\dexes_clearing\DexesClearing;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class H2MExceptionRedirectSubscriber.
 *
 * This subscriber checks for H2MApiKeyNotFoundInCacheException exceptions in KernelEvents
 * and handles the exception accordingly, by starting the H2MFlow for the currently logged in user.
 */
final class H2MExceptionRedirectSubscriber implements EventSubscriberInterface
{
  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array
  {
    return [
      KernelEvents::EXCEPTION => [
        ['redirectOnException'],
      ],
    ];
  }

  /**
   * Constructs H2MExceptionRedirectSubscriber.
   *
   * @param AuthRepository   $authRepository the AuthRepository used for starting the H2M flow
   * @param CurrentPathStack $currentPath    the CurrentPathStack used to get the current path
   */
  public function __construct(private readonly AuthRepository $authRepository,
                              private readonly CurrentPathStack $currentPath)
  {
  }

  /**
   * Redirects user to the start of the H2M flow when a H2MApiKeyNotFoundInCacheException exception is encountered.
   *
   * @param ExceptionEvent $event The exception event
   */
  public function redirectOnException(ExceptionEvent $event): void
  {
    $exception = $event->getException();

    if (get_class($exception) !== H2MApiKeyNotFoundInCacheException::class) {
      return;
    }

    $currentPath         = $this->currentPath->getPath();
    $absoluteRedirectUrl = Url::fromRoute('dexes_clearing.h2mcallback', [DexesClearing::PREV_LOCATION_PARAM_NAME => $currentPath], ['absolute' => TRUE])->toString(TRUE);
    if (!is_string($absoluteRedirectUrl)) {
      $absoluteRedirectUrl = $absoluteRedirectUrl->getGeneratedUrl();
    }

    $response = $this->authRepository->humanToMachineToken($absoluteRedirectUrl);

    $redirect = new TrustedRedirectResponse($response->getPsrResponse()->getHeader('location')[0], $response->getPsrResponse()->getStatusCode());

    $redirect->send();
  }
}
