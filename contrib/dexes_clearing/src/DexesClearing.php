<?php

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_clearing;

class DexesClearing
{
  public const SETTINGS_KEY = 'dexes_clearing.settings';

  public const LOGGER_CHANNEL = 'dexes_clearing';

  public const PREV_LOCATION_PARAM_NAME = 'sendToPreviousLocation';
}
