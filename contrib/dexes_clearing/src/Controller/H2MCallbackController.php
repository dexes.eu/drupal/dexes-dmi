<?php

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_clearing\Controller;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\dexes_clearing\DexesClearing;
use Drupal\dexes_clearing\Services\H2MClearingSdk;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class H2MCallbackController extends ControllerBase
{
    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container): self
    {
        /** @var CacheBackendInterface $cacheBackend */
        $cacheBackend = $container->get('cache.default');

        /** @var H2MClearingSdk $clearingSdk */
        $clearingSdk = $container->get('dexes_clearing.h2msdk');

        return new self($cacheBackend, $clearingSdk);
    }

    /**
     * Constructs H2MCallbackController.
     *
     * @param CacheBackendInterface $cacheBackend The cache backend
     * @param H2MClearingSdk        $clearingSdk  The H2MClearingSDK
     */
    public function __construct(private readonly CacheBackendInterface $cacheBackend,
                                private readonly H2MClearingSdk $clearingSdk)
    {
    }

    /**
     * Cache a H2M token and return 303 redirect response.
     *
     * @param Request $request The H2M token to be cached
     *
     * @return RedirectResponse The redirect response to the location the user came from when he requested the H2M token
     */
    public function cacheToken(Request $request): RedirectResponse
    {
        $token   = 'Bearer ' . $request->query->get('token');
        $prevUrl = $request->query->get(DexesClearing::PREV_LOCATION_PARAM_NAME);

        $this->cacheBackend->set($this->clearingSdk->buildCacheKey(), $token);

        return new RedirectResponse($prevUrl, 303);
    }
 }
