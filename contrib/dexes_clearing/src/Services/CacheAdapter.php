<?php

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_clearing\Services;

use DateInterval;
use Drupal\Core\Cache\CacheBackendInterface;
use Psr\SimpleCache\CacheInterface;

/**
 * Class CacheAdapter.
 *
 * Acts as an adapter for the Drupal Cache Backend.
 */
class CacheAdapter implements CacheInterface
{
  /**
   * CacheAdapter constructor.
   *
   * @param CacheBackendInterface $drupalCache the Drupal cache backend
   */
  public function __construct(private CacheBackendInterface $drupalCache)
  {
  }

  /**
   * {@inheritdoc}
   */
  public function get(string $key, mixed $default = NULL): mixed
  {
    $cache = $this->drupalCache->get($key);

    //@phpstan-ignore-next-line
    return empty($cache) ? $default : $cache->data;
  }

  /**
   * {@inheritdoc}
   */
  public function set(string $key, mixed $value, DateInterval|int|null $ttl = NULL): bool
  {
    if (NULL === $ttl) {
      $this->drupalCache->set($key, $value);
    } else {
      //@phpstan-ignore-next-line
      $this->drupalCache->set($key, $value, time() + $ttl);
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(string $key): bool
  {
    $this->drupalCache->delete($key);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function clear(): bool
  {
    $this->drupalCache->deleteAll();

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getMultiple(iterable $keys, mixed $default = NULL): iterable
  {
    $result = [];
    $k      = (array) $keys;
    $caches = $this->drupalCache->getMultiple($k);
    foreach ($keys as $key) {
      if (!empty($caches[$key])) {
        $result[$key] = $caches[$key]->data;
      } else {
        $result[$key] = $default;
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function setMultiple(iterable $values, DateInterval|int|null $ttl = NULL): bool
  {
    $items = [];
    foreach ($values as $key => $value) {
      if (NULL === $ttl) {
        $items[$key] = ['data' => $value];
      } else {
        //@phpstan-ignore-next-line
        $items[$key] = ['data' => $value, 'expire' => time() + $ttl];
      }
    }
    $this->drupalCache->setMultiple($items);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteMultiple(iterable $keys): bool
  {
    $this->drupalCache->deleteMultiple((array) $keys);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function has(string $key): bool
  {
    $cache = $this->drupalCache->get($key);

    return !empty($cache);
  }
}
