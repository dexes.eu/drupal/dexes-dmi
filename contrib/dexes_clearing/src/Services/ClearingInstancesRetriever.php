<?php

declare(strict_types=1);

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_clearing\Services;

use Dexes\ClearingSdk\ClearingSdk;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\dexes_clearing\DexesClearing;
use Drupal\user\Entity\User;
use Psr\SimpleCache\InvalidArgumentException;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class ClearingInstancesRetriever.
 *
 * A service for retrieving clearing instances.
 */
readonly class ClearingInstancesRetriever
{
  /**
   * Creates an ClearingInstancesRetriever.
   *
   * @param ClearingSdk                   $clearingSdk   The clearing sdk for interacting with policies
   * @param ConfigFactoryInterface        $configFactory The factory for retrieving the relevant config
   * @param AccountProxyInterface         $currentUser   The current user for retrieving his EORI
   * @param LoggerChannelFactoryInterface $loggerFactory The logger factory to creating a logger channel
   */
  public static function create(ClearingSdk $clearingSdk, ConfigFactoryInterface $configFactory, AccountProxyInterface $currentUser, LoggerChannelFactoryInterface $loggerFactory): self
  {
    $clearingConfig = $configFactory->get(DexesClearing::SETTINGS_KEY);
    $user           = User::load($currentUser->id());

    return new self(
      $clearingSdk,
      $clearingConfig->get('clearing_enabled')               ?? FALSE,
      $user?->get('field_company_id')?->get(0)?->getString() ?? '',
      $loggerFactory->get(DexesClearing::LOGGER_CHANNEL)
    );
  }

  /**
   * ClearingInstancesRetriever constructor.
   *
   * @param ClearingSdk            $clearingSdk     The clearing sdk for interacting with policies
   * @param bool                   $clearingEnabled Whether to display clearing related features
   * @param string                 $userEori        The EORI of the current user
   * @param LoggerChannelInterface $logger          The logging channel to use
   */
  public function __construct(private ClearingSdk $clearingSdk,
                              private bool $clearingEnabled,
                              private string $userEori,
                              private LoggerChannelInterface $logger)
  {
  }

  /**
   * Gets the relevant clearing instances for the given dataset.
   *
   * @param array<string, mixed> $dataset The dataset
   *
   * @return array{'requester': array<string, mixed>, 'entitledParty': array<string, mixed>} Returns the clearing instances relevant for this dataset
   */
  public function getClearingInstancesForDataset(array $dataset): array
  {
    if (!$this->clearingEnabled || empty($this->userEori)) {
      return [
        'requester'     => [],
        'entitledParty' => [],
      ];
    }

    $policies = [];
    if (!empty($dataset['attributes']['odrl:hasPolicy']['id'])) {
      $policies[] = $dataset['attributes']['odrl:hasPolicy']['id'];
    }
    foreach ($dataset['relationships'] as $distribution) {
      if (!empty($distribution['attributes']['odrl:hasPolicy']['id'])) {
        $policies[] = $distribution['attributes']['odrl:hasPolicy']['id'];
      }
    }
    $policies = array_unique($policies);

    return $this->getClearingInstances($policies);
  }

  /**
   * Gets the relevant clearing instances for the given dataService.
   *
   * @param array<string, mixed> $dataService The dataService
   *
   * @return array{'requester': array<string, mixed>, 'entitledParty': array<string, mixed>} Returns the clearing instances relevant for this dataService
   */
  public function getClearingInstancesForDataService(array $dataService): array
  {
    if (!$this->clearingEnabled || empty($this->userEori)) {
      return [
        'requester'     => [],
        'entitledParty' => [],
      ];
    }

    $policies = [];
    if (!empty($dataService['attributes']['odrl:hasPolicy']['id'])) {
      $policies[] = $dataService['attributes']['odrl:hasPolicy']['id'];
    }

    return $this->getClearingInstances($policies);
  }

  /**
   * Gets the relevant clearing instances for the given policies.
   *
   * @param string[] $policies A list of policy ids
   *
   * @return array{'requester': array<string, mixed>, 'entitledParty': array<string, mixed>} Returns the clearing instances relevant for the given policies
   */
  private function getClearingInstances(array $policies): array
  {
    $clearingInstances = [
      'requester'     => [],
      'entitledParty' => [],
    ];

    try {
      $clearingInstances['requester'] = $this->clearingSdk->clearing()->requester($this->userEori);
      $clearingInstances['requester'] = array_filter($clearingInstances['requester'], function(array $clearingInstance) use ($policies) {
        return in_array($clearingInstance['offer_policy']['id'], $policies);
      });
      $clearingInstances['entitledParty'] = $this->clearingSdk->clearing()->entitledParty($this->userEori);
      $clearingInstances['entitledParty'] = array_filter($clearingInstances['entitledParty'], function(array $clearingInstance) use ($policies) {
        return in_array($clearingInstance['offer_policy']['id'], $policies);
      });
    } catch (InvalidArgumentException|ClientException|ResponseException $e) {
      $this->logger->error('An error occurred while retrieving clearing instances: ' . $e->getMessage());
    }

    return $clearingInstances;
  }
}
