<?php

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_clearing\Services;

use Dexes\ClearingSdk\ClearingSdk;
use Dexes\ClearingSdk\HttpRequestService;
use Dexes\ClearingSdk\Repositories\Auth\AuthRepository;
use Dexes\ClearingSdk\Repositories\ClearingService\ClearingRepository;
use Dexes\ClearingSdk\Repositories\PolicyStore\Dataspace\OfferPolicyRepository;
use Dexes\ClearingSdk\Repositories\PolicyStore\Dataspace\PolicyTemplateRepository;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;

/**
 * Class H2MClearingSDK.
 *
 * This version of the ClearingSDK is specialized for a H2M flow.
 * The main difference compared to the regular ClearingSDK is the addition
 * of a user ID and a cache key that will be used to store a H2M API key in cache.
 */
class H2MClearingSdk extends ClearingSdk
{
    public const CACHE_PREFIX = 'clearing-h2m-';

    /**
     * H2MClearingSDK constructor.
     *
     * @param HttpRequestService $httpRequestService the HttpRequestService object for making HTTP requests
     * @param AuthRepository     $authRepository     the AuthRepository object for handling authentication
     * @param LoggerInterface    $logger             the LoggerInterface object for logging messages
     * @param CacheInterface     $cache              the CacheInterface object for caching data
     * @param string             $userId             the user ID of the user which we use to create a h2m cache key
     */
    public function __construct(HttpRequestService $httpRequestService,
                                AuthRepository $authRepository,
                                LoggerInterface $logger,
                                CacheInterface $cache,
                                private string $userId)
    {
        parent::__construct($httpRequestService, $authRepository, $logger, $cache);
    }

    /**
     * Creates a new instance of the PolicyTemplateRepository.
     *
     * @return PolicyTemplateRepository the newly created PolicyTemplateRepository instance
     */
    public function makePolicyTemplateRepository(): PolicyTemplateRepository
    {
        return new PolicyTemplateRepository($this->httpRequestService, $this->authRepository, $this->logger, $this->cache, $this->buildCacheKey());
    }

    /**
     * Create a new instance of OfferPolicyRepository.
     *
     * @return OfferPolicyRepository the newly created OfferPolicyRepository instance
     */
    public function makeOfferPolicyRepository(): OfferPolicyRepository
    {
        return new OfferPolicyRepository($this->httpRequestService, $this->authRepository, $this->logger, $this->cache, $this->buildCacheKey());
    }

    /**
     * Creates a new instance of ClearingRepository.
     *
     * @return ClearingRepository The newly created ClearingRepository instance
     */
    public function makeClearingRepository(): ClearingRepository
    {
        return new ClearingRepository($this->httpRequestService, $this->authRepository, $this->logger, $this->cache, $this->buildCacheKey());
    }

    /**
     * Build a cache key used to store a H2M API key for a user.
     *
     * The cache key consists of some cache prefix and the current user ID.
     */
    public function buildCacheKey(): string
    {
        return sprintf('%s%s', self::CACHE_PREFIX, $this->userId);
    }
}
