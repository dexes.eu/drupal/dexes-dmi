# Changelog

## Unreleased (`dev-main`)

### Added

### Changed

- Change cache refreshing to once a day for `list` and `party` in `IshareParticipantsController`.

### Fixed

---

## Release 0.19.0

### Added

- Added new `isCommonPolicy` function in `PolicyDetailsTrait` to check if a offer policy contains a common policy template.

---

## Release 0.18.0

### Changed

- Added new `dexes_pdp` module for the communication with the pdp.

---

## Release 0.17.2

### Changed

- User is now redirected to the page they came from.
- The start date column has been added to the participants list table.

---

## Release 0.17.1

### Fixed

- Start a session before redirecting to the idp, otherwise the session variables won't be saved.

---

## Release 0.17.0

### Changed

- When a user clicks on the login button automatically redirect the user to the idp.

---

## Release 0.16.4

### Fixed

- Add the correct cache keys and tags to the `InitiateServiceClearingForm`.

---

## Release 0.16.3

### Fixed

- Only disable initiate clearing button in `InitiateServiceClearingForm` when the request is for this service.

---

## Release 0.16.2

### Changed

- Added the type and the id of the service to the `InitiateServiceClearingForm`.

---

## Release 0.16.1

### Added

- Added a form which initiates a request in the clearing.

### Changed

- Add support for services in the `policy access modal`.

---

## Release 0.16.0

### Added

- Add the id of a user in the idp to be stored in the user attributes.
- Add a callback endpoint setting to the `ClearingAdminSettingsForm`.

---

## Release 0.15.1

### Fixed

- Link to the correct participant detail page route.   

---

## Release 0.15.0

### Added

- Created a page for viewing iShare members.
- Added a detail page for iShare members.

---

## Release 0.14.0

### Changed

- The DMI logo for policies makes use of the resized version to better fit the layout.
- Change the buttons for more information in the policy template to links.

---

## Release 0.13.0

### Changed

- Update `dexes/clearing-sdk` dependency.

---

## Release 0.12.0

### Added

- A new setting field for Service Consumer detail pages

---

## Release 0.11.0

### Changed

- Replace the badges of a policy with logos in the policy modals.

---

## Release 0.10.0

### Added

- Add a service for retrieve clearing instances for a dataset and a data service.
- Provide a template for displaying clearing instances on the dataset and data service detail pages.

---

## Release 0.9.0

### Added

- Add a setting for a URL to allow a user to create an offer policy in clearing.
- Add a setting for a URL to allow a user to view an offer policy in clearing.
- Add a setting to enable and disable the clearing functions.

---

## Release 0.8.0

### Added

- H2MClearingSDK class
- H2MClearingSDK factory method
- H2MCallbackController used for caching H2M Bearer tokens
- H2MExceptionRedirectSubscriber to start H2M flow when encountering a H2MApiKeyNotFoundInCacheException
- Previous Location Header name constant in DexesClearing

### Changed

- Remove flawed `install/dexes_clearing.settings.yml`.
- Updated various composer dependencies.

---

## Release 0.7.0

### Changed

- Add the url to the detail page of a clearing request as a setting to the `ClearingAdminSettingsForm`.

---

## Release 0.6.0

### Changed

- Updated various composer dependencies

---

## Release 0.5.0

### Changed

- Update the policy form trait to prefill entitled_party and service_provider based on another field.
- Improve the logging of errors that occur while creating an offer policy. 

---

## Release 0.4.0

### Changed

- Update the policy traits and templates in the dexes_policies module to allow the forms to be migrated to DCAT2.

### Fixed

- Removed composer-registry ci job
- OpenID login form and user login form centered, iSHARE button CSS added (CSS is defined in Dexes theme)

---

## Release 0.3.3

### Fixed

- Added all modules to the autoload section of the composer.json file

___

## Release 0.3.2

### Fixed

- The module "dexes_policies" now displays the policy facts instead of the policy validations in the modals.

___

## Release 0.3.1

### Changed

- The module "dexes_policies" now has a dependency on "dexes_clearing".

___

## Release 0.3.0

### Added

- The module "dexes_policies" has been added to help with the display and editing of policies.

___

## Release 0.2.3

### Changed

- Use the mapping openid_connect provides for extra fields.

___

## Release 0.2.2

### Changed

- When a user uses the iSHARE openid login, add company_id and company_name to the user fields.
