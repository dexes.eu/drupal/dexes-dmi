<?php

declare(strict_types=1);

/**
 * This file is part of the dexes-drupal/dexes_dmi package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Drupal\dexes_dmi;

trait GenerateX5cArrayTrait
{
  /**
   * Gets the client certificate chain and formats it as a x5c array.
   *
   * @return array The formatted x5c array
   */
  private static function getX5CArray(string $x5c): array
  {
    $arr = $x5c;
    $arr = str_replace("\r", '', $arr);
    $arr = str_replace("\n", '', $arr);
    /** @var string $arr */
    $arr = str_replace('-----END CERTIFICATE-----', '', $arr);
    $arr = explode('-----BEGIN CERTIFICATE-----', $arr);

    return array_values(array_filter($arr));
  }
}
